#! /bin/bash
echo
echo "----------------------------------------------"
echo
echo "Update Script for Ubuntu Linux, all flavors."
echo "Brought to you by Frank McCourry"
echo
echo "Edit this script to modify for you own needs."
echo "See the script for notes."
echo
echo "----------------------------------------------"

# Alias for apt to substitute nala.  Change comments if you prefer apt.
# apt='apt'
apt='nala'


echo "----------------------------------------------"

# Update apt
echo -e "\a\033[93mUpdating APT packages\e[0m"
sudo $apt update
echo -e "\a\e[93mRunning APT Upgrades\e[0m"
sudo $apt upgrade
echo -e "\a\e[93mCleaning Up APT\e[0m"
sudo $apt autoremove
echo
echo "----------------------------------------------"
echo
# Update Snaps
echo -e "\a\033[93mUpdating Snaps \e[0m"
sudo snap refresh
echo
echo "----------------------------------------------"
echo
# Update FlatPaks
echo -e "\a\033[93mUpdating FlatPaks \e[0m"
sudo flatpak update -y
echo
echo "----------------------------------------------"
echo
# Update Bash Line Editor
echo -e "\a\033[93mUpdating Bash Line Editor \e[0m"
bash ~/.local/share/blesh/ble.sh --update
echo
echo "----------------------------------------------"
echo
# Updates are completed
echo -e "\a\033[93mUpdates are complete \e[0m"
echo
